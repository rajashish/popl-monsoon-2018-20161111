#lang racket
(require eopl)

(define-datatype bst bst?
  [null-node]
  [node (n number?) (l bst?) (r bst?)])

(define (build-tree ls)
    (define (bst-tree v tree)
      (cases bst tree
        [null-node () (node v (null-node) (null-node))]
        [node (n l r)
              (cond
                [(> v n) (node n l (bst-tree v r))]
                [else (node n (bst-tree v l) r)])]))
    (foldl bst-tree (null-node) ls))

(provide build-tree)
(provide bst)
(provide bst?)
(provide node)
(provide null-node)